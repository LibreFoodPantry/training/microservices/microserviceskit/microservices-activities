# Exploring REST API Implementation

## Content Learning Objectives

After completing this activity, students should be able to:

*

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

For this activity, you should have a *Technician* role. The Technician will be the one to have Visual Studio Code running, and the repositories open in Dev Containers. You may want to make sure your Technician is a team member with a more powerful computer.

You can combine the Technician role with another role if you do not have enough team members.

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Technician |
Presenter |
Recorder |
Reflector |

## Prerequisites

You should have a [MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit) GitLab subgroup that has been created for you (either by your instructor, your team leader, or yourself). It will contain a number of respositories. (These activities are likely in a Microservices Activities repository in this subgroup.)

If not, you should follow the instructions in the [README for the MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit/-/blob/main/README.md) to create one.

You need to have the GuestInfoBackend repository cloned to your computer to work on this activity.

## Model 1 - Project Structure

### Open the GuestInfoBackend Repository in Visual Studio Code

1. Follow the instructions in the GuestInfoBackend project's README.md to open
the project either locally or in Gitpod.
2. Open the README file in the GuestInfoBackend repository inside the MicroservicesKit, and read the file.
    * You are reading to get an overview of the project and what tools and documentation are available.
    * Follow the link for the Developer Cheat Sheet to see what the various developer commands are used for.
    * You do not need to follow the rest of the links in the document, but take note of what is there.

The GuestInfoBackend is a *service* written in [Node.js](https://nodejs.org) using the [Express.js](https://expressjs.com) framework.

> Node.js® is an open-source, cross-platform JavaScript runtime environment.
>
> &mdash; [Node.js](https://nodejs.org)

JavaScript code is not compiled, it is interpreted by a program that reads JavaScript statements and executes them directly without compiling the entire program. JavaScript was designed to be interpreted by your web browser, but developers wanted to build JavaScript programs to run on servers, so Node.js was developed.

When GuestInfoBackend is built, the JavaScript source code is copied into a directory in the Docker image, and the location of the code is passed to the Node.js runtime to be run when the server starts.

> Fast, unopinionated, minimalist web framework for Node.js
>
> &mdash; [Express.js](https://expressjs.com)

The Express.js framework provides web server functionality for Node.js. This means that we do not need to write all of the web server functions for ourselves.

### Questions - Model 1

1. List the developer commands.
2. Where would you find the explanation of what each command is used for?
3. If you have changed some of the source files for the server and want to restart the server so that it now uses the new code, which command would you choose?
4. Where can you find the OpenAPI specification for the server?
5. Where is the source code for the server located?

## Model 2 - Source Code Structure - `src` directory

Open the `src` directory. This is where the source code for the server is located. To make changes to the behavior of the server you should only have to change files in this directory.

### Questions - Model 2

1. List the directories (name only) and files in the `src` directory.
2. The `Dockerfile` specifies how to build the Docker image for the server. The details are unimportant for this activity but, in short, it determines which OS is used, copies our files into the Docker image, install Node.js modules used by our code, and starts the server.
3. Open the `docker-compose` file.
    1. List the `services`.
    2. Compare the services you listed to the diagram in MS01 - Model 3. Describe the correspondence between them - what matches, what is missing?
    3. What is the purpose of the `docker-compose` file?
4. `package.json` and `package-lock.json` are used to specify which Node.js modules we want installed for our source code to use.
    1. Open `package.json` what to you notice about the packages listed under `dependencies` and `devDependencies`? Does this look like something you are familiar with?
    2. What is different from what you expected?
    3. Look at [https://michaelsoolee.com/npm-package-tilde-caret/](https://michaelsoolee.com/npm-package-tilde-caret/) and explain the version number for `express`.
        1. Give two examples of version upgrades that would be allowed.
        2. Give an example of a version upgrade that would not be allowed.
5. `index.js` is the file that is run when the server starts. Open the file and describe at a very high level, what the code does. You are probably unfamiliar with JavaScript code, but you should be able to get a feel for the major steps the code takes.

## Model 3 - Source Code Structure - `src/lib` directory

Open the `src/lib` directory.

### Questions - Model 3

1. List the files in the `src/lib` directory.
2. Where have you already seen these file names?
3. Open `config.js`. Read thorough it to get a feel for what it is used for. What are the various configuration variables used for?
4. Open `database.js`.
    1. What design pattern do you see being used here? Why are we using that?
    2. Notice the `async` and `await` keywords in use here. These are used to allow *asynchronous* calls.

        An asynchronous call returns a *promise* that it will supply a value sometime in the future. This is common when you are making a call to something like a database over a network. Normally with a promise, your code moves on to the following code before a value has returned. When the value is returned, some *callback* function that was attached to the promise is called to handle the return value.

        If we put an `await` in front of an asynchronous call, then the code will wait until the promise returns.

        Putting `async` in front of a function turns it into an asynchronous function.
5. Open `logger.js`. What is this file doing?
6. Open `messageBroker.js`. What is this file doing?
7. Open `mount-endpoints.js`. This file builds a data structure which is essentially a table of endpoint path and endpoint method pairs. It finds this information by scanning all the files in the `src/endpoints` directory.
    1. How easy does this make it to add new endpoints? What do we have to modify to add a new endpoint?
    2. What OO design principle that we looked at is being used here?

## Model 4 - Source Code Structure - `src/data` directory

Open the `src/Data/guest.js` file. This file provides Javascript functions to make calls to the database. These functions hide the details of making database requests.

Most of the database operations take a *query* which is what we want to match for the records we want. This is like the `WHERE` clause in SQL. The query looks like:

```json
{ wsuID: id }
```

Some of the database operations also take a *projection* which determines what key/value pairs are returned. This is like the `SELECT` clause in SQL. The query looks like:

```javascript
// { projection: { _id: 0 } } does not return _id field
{ projection: { _id: 0 }}
```

The `find` operation returns multiple records in a *cursor* which is like a Java iterator. So we convert it to an array with `.toArray()`

### Questions - Model 4

1. The following functions will be used by the functions that implement the API endpoints. Explain what each does.
    1. `existsInDB(id)`
    2. `getAll()`
    3. `getOne(id)`
    4. `create(guestData)`
    5. `update(id, guestData)`
    6. `deleteOne(id)`
2. `getGuestsCollection()`
    1. Explain the first line.
    2. Explain the second line.

## Model 5 - Source Code Structure - `src/endpoints` directory

Open the `src/endpoints` directory.

Each JavaScript file represents an endpoint and provides an object with three parts:

* `method` - the HTTP method
* `path` - the endpoint path
* `handler` - the code to handle the request. Each handler is passed the `request`, and returns the `response`.

### Questions - Model 5

1. Look at the list of files in the `src/endpoints` directory. Where have you seen these names before? (Hint: look at `specification/openapi.yaml`)
2. Look at each of the JavaScript files and explain what it does and how it does it.
    1. `createGuest.js`
    2. `deleteGuest.js`
    3. `getAPIVersion.js`
    4. `listGuests.js`
    5. `replaceGuest.js`
    6. `retrieveGuest.js`
3. Based on your review of each of the endpoint files, what fields can you determine that the `request` contains? List them.

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
