# Implementing New REST API Calls

## Content Learning Objectives

After completing this activity, students should be able to:

*

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

For this activity, you should have a *Technician* role. The Technician will be the one to have Visual Studio Code running, and the repositories open in Dev Containers. You may want to make sure your Technician is a team member with a more powerful computer.

You can combine the Technician role with another role if you do not have enough team members.

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Technician |
Presenter |
Recorder |
Reflector |

## Prerequisites

You should have a [MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit) GitLab subgroup that has been created for you (either by your instructor, your team leader, or yourself). It will contain a number of repositories. (These activities are likely in a Microservices Activities repository in this subgroup.)

If not, you should follow the instructions in the [README for the MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit/-/blob/main/README.md) to create one.

You need to have the GuestInfoBackend repository cloned to your computer to work on this activity.

### Open the GuestInfoBackend Repository in Visual Studio Code

Follow the instructions in the GuestInfoBackend project's README.md to open
the project either locally or in Gitpod.

## Model 1: New Endpoint - Get Guest's Age

We would like to have an new endpoint that would allow us to get only a guest's age (rather than all of their information.)

To do so, we need to do two things:

1. Specify the new endpoint in the `specification/openapi.yaml` file.
2. Add a new JavaScript file to implement the endpoint's behavior in the backend.

### New OpenAPI specification for `GET /guests/{wsuID}/age`:

```yaml
  '/guests/{wsuID}/age':
    description: Age of guest with given id.
    parameters:
      - name: wsuID
        in: path
        required: true
        description: Worcester State University ID of the guest whose info is desired.
        schema:
          $ref: '#/components/schemas/WSUID'
    get:
      description: Gets guest's age.
      operationId: retrieveGuestAge
      x-eov-operation-handler: endpoints
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GuestAge'
        '400':
          $ref: '#/components/responses/BadRequest'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/ServerError'
```

### Questions - Model 1

1. Add the OpenAPI fragment in the model above to your `specification/openapi.yaml` file.
    1. Where did you insert it into `specification/openapi.yaml` (which line number)? Why did you insert it there? Could you have put in other locations?
    2. Compare it to the specification for `GET /guests/wsuID` in the same file. List the differences (there are 5 lines that have changed.) Explain each of the changes.

        1.
        2.
        3.
        4.
        5.
2. Make a copy of `src/endpoints/retrieveGuest.js` and name it `src/endpoints/retrieveGuestAge.js`. How did we know to use that file name?
3. In the file, rename the all instances of `retrieveGuest` to `retrieveGuestAge`. Which lines? (there are 3) Explain the why the change was made for each.

    1.
    2.
    3.
4. Modify the `path` on line 6. What is the new path?
5. Modify the `200` response to return the correct data. (In JavaScript, the dot operator (`.`) is used to access a member inside an object.) What does your new response data look like?
6. Use `bin/rebuild.sh` to rebuild the backend and start the server.
7. In `testing/manual/calls.http`, use existing calls and a new call that you must write, to make sure that your new endpoint functions correctly. If it does not, debug your code until it works.
    1. What is the new call you wrote?
    2. What is the response you received?

## Model 2: New Endpoint - Get Guest's Assistance Object

A new endpoint to get only a guest's Assistance object.

### Questions - Model 2

1. Modify the `specification/openapi.yaml` file to add the specification for the new endpoint.
2. Add and modify the new endpoint JavaScript file to add the implementation of the endpoint.
3. Rebuild the backend server.
4. Test the new endpoint.

## Model 3: New Endpoint - Get Guest's Social Security Status

A new endpoint to get only a guest's Social Security status.

### Questions - Model 3

1. Modify the `specification/openapi.yaml` file to add the specification for the new endpoint.
    For the 200 response, you can either define a new schema for socSec and use a $ref to it, or you can just use `type: boolean`.
2. Add and modify the new endpoint JavaScript file to add the implementation of the endpoint.
3. Rebuild the backend server.
4. Test the new endpoint.

## Model 4: New Endpoint - Replace Guest's Assistance Object

A new endpoint to replace the Guest's Assistance object. The advantage to this is that we will not need to pass the full Guest information object in the request body (repeating information that is not changing.) We will only need to pass the Assistance object in the request body. You should return the full guest in the response body.

### Questions - Model 4

1. What HTTP method should we be using for this endpoint? Explain why you chose this HTTP method.
2. Modify the `specification/openapi.yaml` file to add the specification for the new endpoint.
3. Add and modify the new endpoint JavaScript file to add the implementation of the endpoint.
4. Rebuild the backend server.
5. Test the new endpoint.

## Model 5: New Endpoint - Modify Guest's Assistance Object

A new endpoint to modify the Guest's Assistance object. The advantage to this is that we will not need to pass the full Guest assistance object in the request body (repeating information that is not changing.) We pass an Assistance object **with only the fields that are changing** in the request body. You should return the full guest in the response body.

One of the challenges for this is that we need a new schema. The `Assistance` schema we already have lists all fields as `required`. Let's make a new copy of `Assistance` called `AssistanceOptional` and remove the `required` section.

The other challenge is figuring out which of the fields are in the `AssistanceOptional` object passed in the request body. The brute force way to do this would be to use 9 `if` statements to check for the existence of each field. This seems like lots of repetition and difficult to change if new fields are added to the schema.

Fortunately, JavaScript provides a `for...in` statement [(docs)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in) that allows you to iterate over all of the properties (fields) in an object. So, we can do something like:

```javascript
// ...
const id = request.params.id;
const assistanceData = request.body;
var guest = await Guests.getOne(id);
if (guest !== null) {

  for (const property in assistanceData) {
    guest.assistance[`${property}`] = assistanceData[`${property}`];
  }
// ... more code including updating the guest, sending the changes to the message broker, and returning the response
```

``` `${property}` ``` is a template string that lets you convert the value of the variable `property` to a string. That string value is then used as a key to access a field in the object.

### Questions - Model 5

1. What HTTP method should we be using for this endpoint? Explain why you chose this HTTP method.
2. Modify the `specification/openapi.yaml` file to add the specification for the new endpoint and the new schema.
3. Add and modify the new endpoint JavaScript file to add the implementation of the endpoint. Base it on `replaceGuest.js`.
4. Rebuild the backend server.
5. Test the new endpoint.

## Model 6: Add a Query Parameter to Filter Guests

When you make the API calls `GET /guests` you get all the guests in the collection.

But, what if we wanted only a subset of the guests?

There are two ways we could handle this:

1. Filter in Client: In your client code, get all the guests and let your client code filter for the guests you want.
2. Filter in Backend: Add a new endpoint to the backend that returns only the guests you want.

### Questions - Model 6

1. Compare the two methods for filtering the guests - Filter in Client, and Filter in Backend. List advantages and disadvantages for each.

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
