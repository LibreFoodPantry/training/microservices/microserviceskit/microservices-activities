# Exploring the Implementation of a REST API Front End

## Content Learning Objectives

After completing this activity, students should be able to:

* Identify commands needed to start a Vue.js Frontend with a backend/database/message queue test harness
* Explain the difference between production and development versions
* Explain the difference between structure (HTML), style (CSS), and behavior (Javascript) code
* Explain the source code structure of a Vue.js application
* Make simple changes to a Vue.js application

## Process Skill Goals

During the activity, students should make progress toward:

* Reading existing code
* Teamwork

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Open GuestInfoFrontend in Gitpod

1. Go to the GuestInfoFrontend under your team's area in the course. It will be in MicroservicesKit-TheasPantry > GuestInfoSystem > GuestInfoFrontend.
2. Open it in Gitpod.

## Read the Developer Cheat Sheet

### Development vs. Production environments

The development environment is where you work on code. This environment is optimized to make it easier on the developer to make changes and observe the effect of those changes. It uses local or dummy data, and any testing or work you do here will not affect the running (production) version of the code or data that the users see.

The production environment is the long-running version of code and data, and is optimized for performance and user experience.

### Building vs. Hot Reloading

When we make changes that are intended for production, we will build a version of the code that can be deployed to the production environment. In the case of Thea's Pantry, that means building a Docker image to serve the code. For the GuestInfoFrontend this contains and NGINX web server with the Vue.js frontend code copied into the `html` directory to be served to the user when they access the web server. 

If we make changes to the code, we need to build a new Docker image, and then start up that Docker image to try it out. (You should be familiar with the server stop/image rebuild/server start cycle from working with making changes to the GuestInfoBackend.)

However, if we are working on development code, we can often *hot reload* new code into the server. This involves running a development server that watches the source code directories, and reload the web frontend everytime a source file is changed. This is much more convenient for development work. The Vite builder that we are using for Vue.js provides hot reloading capabilities.

1. Open and read the `docs/developer/cheat-sheet.md` file.
2. What are the four top-level categories of commands it covers? What do they do?
3. If you are planning to work on the code as a developer, what are the two commands you will need to use?
4. In what order should you issue those commands? Why?

## Start the Frontend in the Hot-Reloading Development Server

1. Start the backend/database/message queue test harness.
2. In a second terminal, start the hot reloading development server.
3. When a pop-up appears in the lower-right telling you `A service is available on port 5173` choose `Open Browser`. (You may have to tell your browser to allow pop-ups.)

## Try out the frontend

1. Check the contents of the backend `guests` collection by making a call from `testing/manual/calls.http`. Are there any guests in the collection?
2. Open a developer console (should be `F12` on most browsers, then choose the `Console` tab).
3. Enter a valid ID and register the guest.
4. Check the contents of the backend `guests` collection again. Is your newly registered guest there?

## Frontend validation of IDs

1. Enter an invalid ID (too long or too short).
2. Does it call the backend? (Check the browser console)
3. Enter an invalid ID (not all digits).
4. Does it call the backend?
5. What does that tell you about where input validation is occurring?

## Edit a guest using the frontend

1. Look up the guest you previously entered.
2. Make some changes to the guest's information and save the changes.
3. Check the contents of the backend `guests` collection to view your changes.

## Project Code Structure

The source code for the frontend is in the `src` directory. The `src` directory has two subdirectories - `frontend` and `server`.

1. Why are there two subdirectories and what is the purpose of each?

## Frontend Code Structure

The GuestInfoFrontend is written using the [Vue.js framework](https://vuejs.org/).
Vue.js is a *progressive* framework, meaning you can use as little or as much of it as you need to, rather than having to use all of the framework's features.

All web frontends are built using three languages:

* HTML - defines the structure of the web page
* CSS - defines the style of the elements on the web page
* Javascript - defines the behavior of the web page including in-browser processing as well as calls to network services

The starting point for any web page is the `index.html` file at the top level of the file structure.

1. Find the `index.html` file. Where did you find it?
2. What do you recognize in the `<head</head>` section?
3. Look at the `<body></body>` section. Where would you find the rest of the page?
4. Look at the `main.js` file. Give a high-level description of what it does.
5. Look at the `App.vue` file.
   1. How was this used by `main.js`?
   2. Where is the rest of the page defined?

A *router* is used for defining which URLs or *paths* connect to which pages or *views*.

1. Look at the `router/index.ts` file. List the paths and which `.vue` files the connect to.
2. Try entering some of the paths in the browser you opened earlier when you started the frontend. You will add each path to the base URL of the server in your browser, which will look something like `https://5173-worcestercs-guestinfofr-xxxxxxxxxx.ws-usyyy.gitpod.io`
3. Which of these paths did you not encounter while testing the frontend earlier?

A `.vue` file contains three sections (each of which may be empty)

* `<script></script>` - contains the Javascript
* `<template></template>` - contains the HTML
* `<style></style>` - contains the CSS

1. Find the `MainLayout.vue` file. Compare it to what you see when you are on the `/` path.  What is its purpose?
2. Find the `LookUpPage.vue` file. Compare it to what you see when you are on the `/` path.
   1. What is its purpose?
   2. What is its relationship to the `MainLayout.vue`?
3. Find the `RegisterPage.vue` file. Compare it to what you see when you are on the `/` path.
   1. What is its purpose?
   2. What is its relationship to the `MainLayout.vue`?
4. Find the `VerifyPage.vue` file. Compare it to what you see when you are on the `/` path.
   1. What is its purpose?
   2. What is its relationship to the `MainLayout.vue`?

A *store* is a proxy for (in this case) the GuestInfoBackend.

1. Look at the `store/wsuStudentStore.ts` file.
   1. What is the purpose of the `const databaseWarehouse`?
   2. Look at the `actions:` section.
      1. How many actions are there?
      2. What do they each do?
      3. Where is this code being executed - the browser or the backend?
      4. How many of these actions make a call to the backend? List them.
      5. Find an example of a `GET` call to the backend.
         1. Which action is it in?
         2. Find where the response is handled. Which responses does it handle?
         3. How does the frontend view change based on the response? What is the mechanism for doing that?
      6. Find an example of a `POST` call to the backend.
         1. Which action is it in?
         2. How is the request built? How does that match what you did with the `calls.http` files you used for testing?
      7. Find an example of a `PUT` call to the backend.
         1. Which action is it in?
         2. How is the request built? How does that match what you did with the `calls.http` files you used for testing?

## Making Changes

1. See if you can change the `Unemployment` label to `Receiving unemployment benefits`.
2. See if you can swap the WSU logo and the LFP logo?

## If you have time remaining

1. Make some other changes - your choice.

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
